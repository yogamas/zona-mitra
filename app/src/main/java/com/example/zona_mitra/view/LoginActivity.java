package com.example.zona_mitra.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.zona_mitra.R;

public class LoginActivity extends AppCompatActivity {

    Button btnSign;
    EditText edtLogin, edtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSign = findViewById(R.id.button_signin);
        edtLogin = findViewById(R.id.input_login_id);
        edtPass = findViewById(R.id.input_login_password);

        btnSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = edtPass.getText().toString();
                String idLog = edtLogin.getText().toString();
                if (pass.equals("admin") && idLog.equals("admin")) {
                    Intent intent = new Intent(LoginActivity.this, OrderCome.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Akun tidak terdaftar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}